Examples of preprocessor usage

Format:
<name>.js       - preprocessor script (goes into <mapfolder>/jspreproc/<name>.js)
<name>.crpl     - script utilizing <name>.js preprocessing (goes into <mapfolder>/scripts/<name>.crpl)
<name>_out.crpl - output of <name>.js preprocessor when ran on <name>.crpl (Do not use, the file only presents an example output that the game's CRPL compiler would receive)

Scripts:
-infix:
    Allows the user to insert an infix statement with the syntax of: `postfix @{infix} postfix`. Obeys operator precedence, supports function calls, parentheses and the following operators: +-*/^
    Examples:
    - @{1+2}
    - @{sin(PI*<-a)}

-indexer:
	Allows the user to index a list for setting/getting with the appropriate syntaxes: `a[b]`/`a[b]=[c]` where `a` is the list, `b` is the index (and `c` is the element to set). Can be nested.
	Examples:
	- a[0] Trace
	- a[I]=[I I mul]
	- a[I]=[a[I 1 sub] a[I 2 sub] add]