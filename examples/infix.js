var lib =
function() {
	var operators = {}
	var alloperators = {}

	var operator = function() {}
	//operator.prototype = operator

	function newOp(char, precedence, associativity, special)
	{
		var op = new operator()
		op.char = char
		op.pre = precedence
		op.ass = associativity
		op.special = special

		//op.toString = function() { return `[operator ${char}]` }
		//op.toJSON = op.toString
		//op.valueOf = op.toString
		//print(String(op))
		//print(JSON.stringify(op))

		if(!special)
			operators[char] = op
		alloperators[char] = op

		return op
	}

	newOp("+", 2, false)
	newOp("-", 2, false)
	newOp("*", 3, false)
	newOp("/", 3, false)
	newOp("^", 4, true)

	newOp("(", 0, false, true)
	newOp(")", 0, false, true)
	newOp("u-", 5, false, true)
	newOp(",", 0, false, true)
	newOp("call", 0, false, true)

	function parse(input)
	{
		var regex = /([\+\-\*\/\^\(\),])|((?:<\-|\->|-\?)?[^\s*+\-\/()]+)/g
		var opstack = []
		var pstack = []

		var outarr = []
		var last

		var match
		while(match = regex.exec(input))
		{
			if(match[1])
			{
				var s = match[1]
				var op
				if(s == "-" && (last instanceof operator) && last.char != ")")
					op = alloperators["u-"]
				else
					op = operators[s]

				if(op)
				{

					var topop
					while(topop = opstack[opstack.length-1])
					{
						if(!op.ass && topop.pre >= op.pre)
						{
							outarr.push(opstack.pop())
						}
						else break;
					}
					opstack.push(op)
				}
				else
				{
					op = alloperators[s]
					if(!op)
					{
						throw new Error("Syntax error: operator is not... operator?")
					}

					switch(s)
					{
						case "(":
							var newlevel = {}
							if((typeof last) == "string")
							{
								newlevel.call = true
								newlevel.callstring = outarr.pop()
							}
							opstack.push(op)
							pstack.push(newlevel)
							break

						case ")":
							var curlevel = pstack.pop()
							if(!curlevel)
							{
								throw new Error("Syntax error: unmatched closing parenthesis")
							}
							if(last instanceof operator)
							{
								if(last.char == "(")
								{
									if(!curlevel.call)
										throw new Error("Syntax error: empty non-call parentheses")
								}
								else if(last.char == ",")
								{
									throw new Error("Syntax error: empty call argument")
								}
							}
							var top
							while(top = opstack.pop())
							{
								if(top.char == "(")
								{
									break
								}
								else
								{
									outarr.push(top)
								}
							}

							if(curlevel.call)
							{
								outarr.push(curlevel.callstring)
							}
							break

						case ",":
							var curlevel = pstack[pstack.length-1]
							if(last instanceof operator)
							{
								if(last.char == "," || last.char == "(")
								{
									throw new Error("Syntax error: empty call argument")
								}
							}

							if(!curlevel.call)
							{
								throw new Error("Syntax error: argument outside of call")
							}

							var top
							while(top = opstack[opstack.length-1])
							{
								if(top.char == "(")
								{
									break
								}
								else
								{
									outarr.push(opstack.pop())
								}
							}

							break
						default:
					}
				}

				last = op
			}
			else if(match[2])
			{
				outarr.push(match[2])
				last = match[2]
			}
		}

		//After regex is done
		var topop
		while(topop = opstack.pop())
		{
			if(topop.special)
			{
				throw new Error("Syntax error: unfinished special operator at EoF")
			}
			outarr.push(topop)
		}

		return outarr
	}

	var lib = {}
	lib.parse = parse
	return lib
}()

var m_infix = /@{([^}]+)}/g
var tokens_to_crpl = {
	"*": "mul",
	"/": "div",
	"+": "add",
	"-": "sub",
	"^": "pow",
}

function main(s)
{
	s = s.replace(m_infix, function(full, a)
	{
		var parsed = lib.parse(a)
		var out = " "

		for (var i = 0, len = parsed.length; i < len; i++) {
			var token = parsed[i]
			if(typeof(token) == "string")
			{
				out = out + token
			}
			else
			{
				out = out + tokens_to_crpl[token.char]
			}
			out = out + " "
		}

		return out
	})

	return s
}