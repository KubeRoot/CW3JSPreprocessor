//PREPROCESSOR SCRIPT BEGIN
var m_getter = /([^\s^[=]+)\[([^\]\[]+)\](?!=)/g
var r_getter = "<-$1 $2 GetListElement"
var m_setter = /([^\s^[=]+)\[([^\]\[]+)\]=\[([^\[\]]+)\]/g
var r_setter = "<-$1 $2 $3 SetListElement"

function main(s)
{
	var olds
	do
	{
		olds = s
		s = s.replace(m_getter, r_getter).replace(m_setter, r_setter)
	}
	while(olds != s)

	return s
}