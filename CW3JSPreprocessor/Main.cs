﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;
using System.Linq;

public class patch_CrplCompiler : CrplCompiler
{
    private struct JSPreprocInstance
    {
        public Jurassic.ScriptEngine engine;
        public Jurassic.Library.FunctionInstance mainfunc;
        public string fullpath;
        public string preprocname;
        public bool isvalid;
        //public byte[] lasthash;
        public string code;
    }

    private static Regex jspreproc_regex = new Regex(@"^#\$jspreproc\s+([\w]+\.js)\s*$", RegexOptions.Multiline | RegexOptions.ECMAScript);
    private static Dictionary<string, JSPreprocInstance> js_cache = new Dictionary<string, JSPreprocInstance>();
    private static SHA1 jspreproc_hasher = new SHA1CryptoServiceProvider();

    private static string DoJSPreproc(string program, string preprocname, string preprocpath)
    {
        JSPreprocInstance instance;
        string fullpath = preprocpath + preprocname;

        Directory.CreateDirectory(preprocpath);
        if (!File.Exists(fullpath))
        {
            return program;
        }
        
        string preproccode = File.ReadAllText(fullpath);
        //byte[] hash = jspreproc_hasher.ComputeHash(new UTF8Encoding().GetBytes(preproccode));

        //if (!js_cache.ContainsKey(preprocname) || hash.SequenceEqual(js_cache[preprocname].lasthash))
        if (!js_cache.ContainsKey(preprocname) || preproccode != js_cache[preprocname].code)
            {
            UnityEngine.Debug.Log("Generating Preprocessor Instance");
            instance = new JSPreprocInstance
            {
                isvalid = false
            };
            instance.fullpath = fullpath;
            instance.preprocname = preprocname;
            //instance.lasthash = hash;
            instance.code = preproccode;
            if (!js_cache.ContainsKey(preprocname))
                js_cache.Add(preprocname, instance);
            else
            {
                js_cache[preprocname] = instance;
            }

            try
            {
                instance.engine = new Jurassic.ScriptEngine();
                instance.engine.ForceStrictMode = true;
                instance.engine.Execute(preproccode);
                instance.mainfunc = instance.engine.GetGlobalValue<Jurassic.Library.FunctionInstance>("main");
            }
            catch(Exception e)
            {
                UnityEngine.Debug.Log(e);
                return program;
            }
            instance.isvalid = true;
            js_cache[preprocname] = instance;
        }

        instance = js_cache[preprocname];
        if (!instance.isvalid)
        {
            UnityEngine.Debug.Log("Preprocessor instance isn't valid");
            return program;
        }

        try
        {
            object ret = instance.mainfunc.Call(instance.mainfunc, new[] { program });
            string value;

            if(ret is Jurassic.ConcatenatedString)
            {
                value = (ret as Jurassic.ConcatenatedString).ToString();
            }
            else if(ret is string)
            {
                value = ret as string;
            }
            else
            {
                return program;
            }

            //UnityEngine.Debug.Log("Processed code:\n\n"+value);

            return value;
        }
        catch
        {
            return program;
        }
    }

    public extern bool orig_Compile(string scriptName, string program, out string resultMessage, out List<CrplCore.Command> commands, out Dictionary<string, int> funcTable, out Dictionary<string, CrplCore.Data> inputVars, bool overwriteCache = false);
    public bool Compile(string scriptName, string program, out string resultMessage, out List<CrplCore.Command> commands, out Dictionary<string, int> funcTable, out Dictionary<string, CrplCore.Data> inputVars, bool overwriteCache = false)
    {
        string worldEditorDir = GameSpace.GetWorldEditorDir();
        if (worldEditorDir != null && worldEditorDir != string.Empty)
        {
            UnityEngine.Debug.Log("Preprocessing " + scriptName);
            string jspreproc_path = worldEditorDir + "/jspreproc/";
            foreach (Match defmatch in jspreproc_regex.Matches(program))
            {
                try
                {
                    program = DoJSPreproc(program, defmatch.Groups[1].Value, jspreproc_path);
                }
                catch(Exception e)
                {
                    UnityEngine.Debug.Log("//Caught exception:\n"+e);
                }
            }
        }

        return orig_Compile(scriptName, program, out resultMessage, out commands, out funcTable, out inputVars, overwriteCache);
    }
}