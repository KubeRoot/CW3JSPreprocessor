CW3JSPreprocessor - Preprocess your scripts with JS from inside the game!

Usage:
When compiling a CRPL script from the map editor, a new folder (jspreproc)
will get created in the map's directory. You can put a .js file in there,
containing the definition for a function main(string) which returns a string.
When a CRPL file contains a line

    #$jspreproc filename.js

the main function from the jspreproc/filename.js file will be called on the
CRPL code as a string and the returned value will be used when compiling the
script instead.

Installation instructions:

- Download [Release.zip from the repo](https://gitlab.com/KubeRoot/CW3JSPreprocessor/raw/master/Release.zip)
- Unpack into Creeper World 3\CW3_Data\Managed
- Run patch.bat
